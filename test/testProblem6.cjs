let BMWAndAudiList = require('./../problem6.cjs');

console.log("The list of Audi and BMW cars is below");

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify article link

for(let index = 0; index < BMWAndAudiList.length; index++){
    console.log(JSON.stringify({id: BMWAndAudiList[index].id, car_make : BMWAndAudiList[index].car_make, car_model : BMWAndAudiList[index].car_model, car_year : BMWAndAudiList[index].car_year}));
}
