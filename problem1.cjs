let carInventory = require('./carData.cjs');

function carInfoWithId(carInventory, Id){
    // to check if array is empty or not an array
    if((Array.isArray(carInventory) == false) || !carInventory ||(carInventory && carInventory.length === 0)){
        return [];
    }
    // to check if Id is valid or not 
    if(typeof Id !== 'number' || (Id !== 0 && !Id) ){
        return [];
    }
    let carInfoId;
    for(let index = 0; index < carInventory.length; index++){
        if(carInventory[index].id === Id){
            carInfoId = carInventory[index];
        }
    }

  return carInfoId;
 }

module.exports = carInfoWithId;


